ewoksorangetemplate |version|
=============================

.. important::

    THIS PROJECT IS OBSOLETE. USE `ewokscookie <https://gitlab.esrf.fr/workflow/ewoksapps/ewokscookie>` INSTEAD.

*ewoksorangetemplate* provides a template for projects that implements Ewoks tasks with Orange widgets.

*ewoksorangetemplate* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
of the `European Synchrotron <https://www.esrf.eu/>`_.

Create new project
------------------

To start a new project called `myproject` based on the template, you first clone the template project

.. code::

    git clone https://gitlab.esrf.fr/workflow/ewoksapps/ewoksorangetemplate.git myproject
    rm -rf myproject/.git

Replace the string `ewoksorangetemplate` with `myproject` in all your files

.. code::

    grep -rl ewoksorangetemplate ./myproject | xargs sed -i 's/ewoksorangetemplate/myproject/g'

Rename all `ewoksorangetemplate` directories with `myproject`

.. code::

    find ./myproject -depth -name "*ewoksorangetemplate*" | \
    while IFS= read -r ent; do mv $ent ${ent%ewoksorangetemplate*}myproject${ent##*ewoksorangetemplate}; done

Register the Orange widgets in the current python environment and run the tests to check that everything works

.. code::

    pip install -e ./myproject[test]  # installs Ewoks, Orange and test utilities
    pytest ./myproject

To build the documentation of your project

.. code::

    pip install -e ./myproject[doc]  # install doc utilities
    sphinx-build ./myproject/doc ./myproject/build/sphinx/html -E -a
    firefox myproject/build/sphinx/html/index.html

Project structure
-----------------

An Ewoks project with Orange widgets typically has this structure

.. code::

    .
    ├── LICENSE.md
    ├── pyproject.toml
    ├── README.md
    ├── setup.cfg
    ├── setup.py
    └── src
        ├── ewoksorangetemplate
        │   ├── __init__.py
        │   ├── tasks
        │   │   ├── __init__.py
        │   │   └── sumtask.py
        │   └── tests
        │       ├── conftest.py
        │       ├── __init__.py
        │       ├── test_sumtask.py
        │       └── test_tutorials.py
        └── orangecontrib
            └── ewoksorangetemplate
                ├── categories  # THIS IS OPTIONAL
                │   ├── examples1
                │   │   ├── icons
                │   │   │   ├── category.svg
                │   │   │   ├── __init__.py
                │   │   │   └── sum.png
                │   │   ├── __init__.py
                │   │   ├── sumtask.py
                │   │   └── tutorials
                │   │       ├── examples1.ows
                │   │       └── __init__.py
                │   ├── examples2
                │   │   ├── icons
                │   │   │   ├── category.svg
                │   │   │   ├── __init__.py
                │   │   │   └── sum.png
                │   │   ├── __init__.py
                │   │   ├── sumtask.py
                │   │   └── tutorials
                │   │       ├── examples2.ows
                │   │       └── __init__.py
                │   └── __init__.py
                ├── icons
                │   ├── category.svg
                │   ├── __init__.py
                │   └── sum.png
                ├── __init__.py
                ├── sumtask.py
                └── tutorials
                    ├── examples.ows
                    └── __init__.py

The template provides

* example ewoks tasks
* example orange widgets for each Ewoks task (one or more categories)
* tutorial workflows
* unit tests for ewoks tasks, orange widgets and tutorial workflows
* sphinx documentation

The modules that implement Ewoks tasks:

* `src/ewoksorangetemplate/tasks/sumtask.py`: example of ewoks tasks (pure computation, no GUI)

The modules that implement Orange widgets:

* `src/orangecontrib/ewoksorangetemplate/sumtask.py`: example of an Orange widget in the main category (see `NAME = "Ewoks Examples"`)
* `src/orangecontrib/ewoksorangetemplate/categories/examples1/sumtask.py`: example of an Orange widget in the category "Examples1" (see `NAME = "Ewoks Examples (1)"`)
* `src/orangecontrib/ewoksorangetemplate/categories/examples1/sumtask.py`: example of an Orange widget in the category "Examples2" (see `NAME = "Ewoks Examples (2)"`)

The `[options.entry_points]` section in `setup.cfg` specifies the modules where Orange widgets and tutorials are located.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
