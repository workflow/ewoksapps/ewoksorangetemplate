"""rm -rf doc/_generated/; sphinx-build doc build/sphinx/html -E -a
"""

from ewoksorangetemplate import __version__ as release

project = "ewoksorangetemplate"
version = ".".join(release.split(".")[:2])
copyright = "2021-2024, ESRF"
author = "ESRF"
docstitle = f"{project} {version}"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.viewcode",
    "sphinx_autodoc_typehints",
]
templates_path = ["_templates"]
exclude_patterns = []

always_document_param_types = True

autosummary_generate = True
autodoc_default_flags = [
    "members",
    "undoc-members",
    "show-inheritance",
]

html_theme = "pydata_sphinx_theme"
html_static_path = []
html_theme_options = {
    "icon_links": [
        {
            "name": "pypi",
            "url": "https://pypi.org/project/ewoksorangetemplate",
            "icon": "fa-brands fa-python",
        },
    ],
    "gitlab_url": "https://gitlab.esrf.fr/workflow/ewoksapps/ewoksorangetemplate",
    "navbar_start": ["navbar_start"],
    "footer_start": ["copyright"],
    "footer_end": ["footer_end"],
}
